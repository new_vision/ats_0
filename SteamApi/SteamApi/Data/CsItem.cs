﻿using System;
namespace SteamApi.Data
{
    public class CSGOItem
    {

		//      public string Name_en
		//      {
		//          get;
		//          set;
		//      }

		//public string Name_ru
		//{
		//	get;
		//	set;
		//}
		public string Name
		{
		  get;
		  set;
		}

        public float Price_buy // maybe float
		{
			get;
			set;
		}

        public float Price_sale
		{
			get;
			set;
		}

        public int Quantity
        {
            get;
            set;
        }


        public CSGOItem(string name, float buy, float sale, int quantity) {
            Name = name;
            Price_buy = buy;
            Price_sale = sale;
            Quantity = quantity;
        }

        public override string ToString()
        {
            return string.Format("[{0}; Buy = {1}; Sale = {2}; Quantity = {3}]",
                                 Name, Price_buy, Price_sale, Quantity);
        }

    }
}
