﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Text.RegularExpressions;
using SteamApi.Data;
using System.Web; 

namespace Test
{
    public class MainClass
    {
        
        public static void Main(string[] args)
        {
            Console.WriteLine("Start!");
            Console.WriteLine(new string('-', 40));
            // Работаем через Selenium, автоматически открываю браузер, направляю на нужную страницу и загружаю ее.
            // Надо попробовать найти более оптимизированный способ, без браузера.
            var browser = new OpenQA.Selenium.Chrome.ChromeDriver();
            var document = new HtmlAgilityPack.HtmlDocument();

            if (browser == null) { // loading... wait...
                Thread.Sleep(3000);
                Console.WriteLine("Loading... Please wait...");
            }

            int total = 10000; // skins count at the moment

            DateTime startTime = System.DateTime.Now;
            Console.WriteLine(startTime);

            for (int i = 0; i < total; i += 100) {

                //Random rand = new Random();
                //Thread.Sleep(rand.Next(500, 5001));

                string url = @"http://steamcommunity.com/market/search/render/" +
                    "?query=&start=" + i + "&count=100&search_description=0&sort_column=quantity&sort_dir=desc&appid=730";
                browser.Navigate().GoToUrl(@url);
                document.LoadHtml(browser.PageSource);
                var text = document.DocumentNode.SelectSingleNode
                                   (@"//pre[@style='word-wrap: break-word; white-space: pre-wrap;']").InnerText;
                text = HttpUtility.UrlDecode(text);
                text = HttpUtility.HtmlDecode(text);

                var regexState = new Regex(@"""success"":true");     // проверка корректности
                bool STATE = regexState.IsMatch(text);

                if (!STATE) {
                    Console.WriteLine("Success = false");
                    break;
                }

                var regexTotalNow = new Regex(@"""total_count"":.*?,""");
                total = Convert.ToInt32(StrCut(regexTotalNow.Match(text).ToString(), ':', ','));
                Console.WriteLine(new string('#', 5));
                Console.WriteLine("Success = " + STATE);
                Console.WriteLine("Total = " + total);
                Console.WriteLine(new string('#', 5));
                Console.WriteLine();



                var regexLink = new Regex
                    (@"href=\\""http:\\/\\/steamcommunity\.com\\/market\\/listings\\/730\\/.*?\\");
                //  href=\"http:\/\/steamcommunity.com\/market\/listings\/730\/Operation%20Wildfire%20Case\"

                var regexPurchasePrice = new Regex(@"""normal_price\\"">.*?\\");
                // "normal_price\">
                // a two main formats: "0,15 p" and "$0.02 USD"

                var regexSalePrice = new Regex(@"""sale_price\\"">.*?\\");
                // "sale_price\">
                // a two main formats: "0,15 p" and "$0.02 USD"

                var regexQuantity = new Regex(@"num_listings_qty\\"">.*?<");
                // num_listings_qty\">
                // 3,113,629

                int nameCount = regexLink.Matches(text).Count;
                int purchaseCount = regexPurchasePrice.Matches(text).Count;
                int saleCount = regexSalePrice.Matches(text).Count;
                int quantityCount = regexQuantity.Matches((text)).Count;


                if (purchaseCount != saleCount || nameCount != purchaseCount
                    || nameCount != quantityCount)
                {
                    Console.WriteLine("Error: count of nameCount, purchaseCount," +
                                      " saleCount, quantityCount aren't equal");
                    Console.WriteLine("Names = " + nameCount);
                    Console.WriteLine("PurchasePrice = " + purchaseCount);
                    Console.WriteLine("SalePrice = " + saleCount);
                    Console.WriteLine("Quantity = " + quantityCount);
                    // refresh page, go on next iteration, var perfectRefresh = false,
                    // add dont refreshed skins to special array, in order to stop deals with this skins.  
                }

                Console.WriteLine("Count: " + nameCount);

                string[] names = new string[nameCount];
                int[] quantities = new int[nameCount];
                float[,] prices = new float[2, nameCount]; //buy, sale
                {
                    // Processing names
                    int q = 0;
                    foreach (Match m in regexLink.Matches(text))
                    {
                        string s = StrCut(m.ToString(), '/', '\\');
                        names[q] = s;
                        q++;
                    }

                    // Processing quantities
                    q = 0;
                    foreach (Match m in regexQuantity.Matches(text))
                    {
                        string s = StrCut(m.ToString(), '>', '<');
                        quantities[q] = Convert.ToInt32(CommasDel(s));
                        q++;
                    }

                    // Processing purchasePrice and salePrice 
                    q = 0;

                    for (Match buy = regexPurchasePrice.Match(text), sale = regexSalePrice.Match(text);
                        buy.Success; buy = buy.NextMatch(), sale = sale.NextMatch())
                    {

                        string[] s = new string[2];
                        s[0] = StrCut(sale.ToString(), '>', '<');
                        s[1] = StrCut(buy.ToString(), '>', '<');

                        // $1,844.00 USD
                        Regex regexUSD = new Regex(@"\$.*?USD"); // currency
                                                                 // 112692,93 p\u0443\u0431.
                        Regex regexRUB = new Regex(@"p\u0443\u0431\."); // eng "p" and rus "уб"

                        for (int j = 0; j < 2; j++)
                        {
                            if (regexUSD.IsMatch(s[j]))
                            {
                                //$0.03 USD
                                s[j] = CommasDel(s[j]);
                                s[j] = s[j].Replace('.', ',');
                                s[j] = StrCut(s[j], '$', ' ');
                            }
                            else if (regexRUB.IsMatch(s[j]))
                            {
                                Console.WriteLine("Error: input RUB currency is not realised");
                            }
                            else
                            {
                                Console.WriteLine("Error: currency is unknown: " + s[j]);
                            }
                        }
                        prices[0, q] = float.Parse(s[0]);
                        prices[1, q] = float.Parse(s[1]);
                        q++;
                    }


                }

                CSGOItem[] items = new CSGOItem[nameCount];

                for (int j = 0; j < items.Length; j++)
                {
                    items[j] = new CSGOItem(names[j], prices[0, j], prices[1, j], quantities[j]);
                    Console.WriteLine(i+j + ". " + items[j].ToString());
                }

			}

            DateTime finishTime = System.DateTime.Now;
            TimeSpan left = finishTime.Subtract(startTime);
            Console.WriteLine(left);

            //browser.Close();
            //browser.Quit();

        }
        public static string StrCut(string s, char begin, char end) {
            //cutting string from s from end to begin
            // maybe need to realise, when instead chars would be strings
            string res = "";
            int[] pos = new int[2];
            for (int i = s.Length - 1; i >= 0; i--) {
                if (s[i] == end)
                    pos[1] = i - 1;
                if (s[i] == begin && pos[1] != 0) {
                    pos[0] = i + 1;
                    break;
                }
            }
            if (pos[0] != 0 && pos[1] != 0) {
                for (int i = pos[0]; i <= pos[1]; i++) {
                    res += s[i];
                }
            }
            else {
                Console.WriteLine("Error in StrCut. Needed chars dont finded.");
            }
            return res;
        }

        public static string CommasDel(string s) {
            string result = "";
            for (int i = 0; i < s.Length; i++) {
                if (s[i] != ',')
                    result += s[i];
            }
            return result;
        }

   
    }
}

